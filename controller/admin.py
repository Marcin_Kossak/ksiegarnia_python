from bottle import Bottle, route, run, template, static_file, request, get, post, view


admin = Bottle()

@admin.route('/')
@view('admin/index')
def index():
    return dict(tytul='Czesc')


@admin.route('/wyloguj')
@view('admin/wyloguj')
def wyloguj():
    return dict(tytul='Czesc')