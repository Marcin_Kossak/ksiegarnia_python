from bottle import Bottle, route, run, template, static_file, request, get, post, view, TEMPLATE_PATH
from controller.admin import admin
import os

folder_path = os.path.join(os.path.dirname(os.path.abspath(__file__)))
os.chdir(folder_path)

app = Bottle()

@app.route('/hello/<name>')
def hello(name):
    return template('<b>Hello {{name}}</b>!', name=name)


@app.route('/')
@view('index')
def index():
    return dict(tytul='Czesc')

@app.route('/static/<filename:path>')
def static_files(filename):
    print(filename)
    return static_file(filename, './static/')

app.mount('/admin', admin)

run(app, host='localhost', port=8080, reloader=True)