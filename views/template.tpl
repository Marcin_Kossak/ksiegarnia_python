<!DOCTYPE html>
<html>
    <head>
        <title>{{title or 'Jeszcze nie ma tytułu'}}</title>
         <link rel="stylesheet" type="text/css" href="/static/bootstrap/css/bootstrap.min.css">
         <script src="/static/jquery-3.3.1.min.js"></script>
         <script src="/static/bootstrap/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="site-navigation" style="width: 100%">
                    <nav class="navbar navbar-expand-lg navbar-light bg-light">
                        <a class="navbar-brand" href="#">Navbar</a>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                          <li class="nav-item active">
                            <a class="nav-link" href="/">Strona główna</a>
                          </li>
                          <li class="nav-item active">
                            <a class="nav-link" href="/admin">Admin</a>
                          </li>
                        </ul>
                    </nav>
                </div>
            </div>

            <br />

            <div class="site-content">
                <div class="card">
                    <div class="card-header">
                        Tytuł jakiś tam na stronie
                    </div>

                    <div class="card-body">
                        {{!base}}
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>